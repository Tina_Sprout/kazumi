﻿using System;
using UnityEngine;

namespace Cheats.Kazumi.Utils {
    public class config {
        //Colors used for console
        public static Color LogLevel_Error = Color.red;
        public static Color LogLevel_Warning = Color.yellow;
        public static Color LogLevel_Assert = Color.green;
        public static Color LogLevel_Execption = new Color(1,0.5f,0,1);
        public static Color LogLevel_Log = Color.white;
        public static int Log_maxamount = 1000;
        public static Boolean Log_AutoScroll = true;
        public static Boolean Console_Show = true;
        public static KeyCode Keybind_Console = KeyCode.Backslash;
        public static string Console_InternalControlerName = "CmdInput";
        public static int Console_WindowID = 420;

    }
}