﻿using System;
using System.Linq;
using System.Reflection;

namespace Cheats.Kazumi.Utils {
    public class ReflectionUtil {
        //Credits goes to: Fredrik Monk on stackexchange
        public static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        } 
    }
}