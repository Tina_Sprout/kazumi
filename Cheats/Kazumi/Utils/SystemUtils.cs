﻿using UnityEngine;

namespace Cheats.Kazumi.Utils {
    public class SystemUtils {
        public static void SetClipboard(string text) {
            TextEditor te = new TextEditor();
            te.text = text;
            te.SelectAll();
            te.Copy();
        } 
    }
}