﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cheats.Kazumi.Handlers {






    public class TextureManager : MonoBehaviour {
        private static Dictionary<string, Texture2D> Textures = new Dictionary<string, Texture2D>();

        public static IEnumerator DownloadTexture(string name, string url) {
            using (WWW www = new WWW(url)) {
                yield return www;

                if (www.texture) {
                    Textures.Add(name, www.texture);
                }
            }
        }

        public static void MakeSolid(string name, byte r, byte g, byte b, byte a = 255) {
            Texture2D tmp = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            tmp.SetPixel(0, 0, new Color32(r, g, b, a));
            tmp.filterMode = FilterMode.Point;
            tmp.wrapMode = TextureWrapMode.Repeat;
            tmp.Compress(false);
            tmp.Apply();

            if (Textures.ContainsKey(name))
                Textures.Remove(name);
            Textures.Add(name, tmp);
        }

        public static void Add(string name, Texture2D texture) {
            Textures.Add(name, texture);
        }

        public static void Delete(string name) {
            if (Textures.ContainsKey(name)) {
                Textures.Remove(name);
            }
        }

        public static Texture2D Get(string name) {
            if (Textures.ContainsKey(name))
                return Textures[name];
            return null;
        }

        public static Texture2D GetRaw(string name) {
            return Textures[name];
        }

        void Awake() {
            MakeSolid("Red", 255, 0, 0);
            MakeSolid("Green", 0, 255, 0);
            MakeSolid("Blue", 0, 0, 255);
        }
    }
}