﻿using System;
using System.Collections.Generic;
using Cheats.Kazumi.Core;
using Cheats.Kazumi.Objects;
using UnityEngine;
using Console = System.Console;

namespace Cheats.Kazumi.Handlers {
    public class ModuleHandler : MonoBehaviour{
        
        public static Dictionary<string, Module> Modules = new Dictionary<string, Module>();

        void Update() {
            foreach (Module module in Modules.Values) {
                module.OnUpdate();
                
            }
        }

        void OnGUI() {
            foreach (Module module in Modules.Values) {
                module.OnRenderFrame();
            }
        }
        
        void Start() {
            Type moduletype = typeof(Module);
            foreach (Type VARIABLE in typeof(Loader).Assembly.GetTypes()) {
                try {
                    if (VARIABLE.IsSubclassOf(moduletype)) {
                        Module module = (Module) Activator.CreateInstance(VARIABLE);
                        Modules.Add(module.moduleInfo.Name.ToLower(), module);
                    }
                }
                catch (Exception e) {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}