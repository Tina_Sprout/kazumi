﻿using System;
using System.Linq;
using System.Reflection;

namespace Cheats.Kazumi.Objects {
    public abstract class Module {
        abstract public void OnDisable();

        abstract public void OnEnable();

        abstract public void OnUpdate();
        abstract public void OnToggle();
        abstract public void OnRenderFrame();
        private bool Enabled = false;
        public void Enable() {
            Enabled = true;
            OnEnable();
        }

        public void Disable() {
            Enabled = false;
            OnDisable();
        }

        public void Toggle() {
            if (Enabled) {
                Disable();
            }
            else {
                Enable();
            }
            OnToggle();
        }
        public ModuleInfo moduleInfo;

        public Module() {
            moduleInfo = GetType().GetCustomAttributes(typeof(ModuleInfo)).FirstOrDefault() as ModuleInfo;
        }
        [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
        public sealed class ModuleInfo : Attribute {
            // See the attribute guidelines at 
            //  http://go.microsoft.com/fwlink/?LinkId=85236
            public readonly string Name;

            private String Category;
            // This is a positional argument
            public ModuleInfo(string Name, String Category = "Not Implimented yet") {
                this.Name = Name;
                this.Category = Category;
                
            }
            
            
           
            public int NamedInt { get; set; }
        }
    }
}