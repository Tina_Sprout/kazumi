﻿using System;
using PlayFab.Json;
using UnityEngine;
using Console = Cheats.Kazumi.Core.Console;

namespace Cheats.Kazumi.Objects {
    public class Argument{
        
        public enum ArgumentType {
            String,
            Integer,
            Boolean,
            Float,
            Double
        }

        public ArgumentType type;
        public String Name;
        public bool Requierd;
        public String About;
        public bool prefix_Required;

        public Argument(String topahrese) {
            string[] args = topahrese.Split(':');
            bool req = bool.Parse(args[1]);
            
            ArgumentType Ttype = ArgumentType.String;
            switch (args[2].ToLower()) {
                    case "s":Ttype =  ArgumentType.String;
                        break;
                    case "i":Ttype =  ArgumentType.Integer;
                        break;
                    case "f":Ttype =  ArgumentType.Float;
                        break;
                    case "d":Ttype =  ArgumentType.Double;
                        break;
                    case "b":Ttype =  ArgumentType.Boolean;
                        break;
            }
            
            this.Name = args[0];
            this.Requierd = req;
            
            
            this.About = args[5];
            this.type = Ttype;
            
            this.RequiresValue = bool.Parse(args[4]);
            this.prefix_Required = bool.Parse(args[3]);

        }

        public bool RequiresValue;
        
        public Argument(String name, bool Required, ArgumentType type, string About) {
            this.Name = name;

            this.Requierd = Required;
            
            this.type = type;
            this.About = About;
            
        }

        
        //for the return of an argument
        private int IValue;

        private String SValue;
        private float FValue;
        private bool BValue;
        private double DValue;
        
        public Argument(String name, string value) {
            this.Name = name;
            this.SValue = value;
        }
        
        public Argument(String name, int value) {
            this.Name = name;
            this.IValue = value;
        }
        
        public Argument(String name, float value) {
            this.Name = name;
            this.FValue = value;
        }
        
        public Argument(String name, bool value) {
            this.Name = name;
            this.BValue = value;
        }
        
        public Argument(String name, double value) {
            this.Name = name;
            this.DValue = value;
        }
  
        
    }
}