﻿using System.Linq;
using Cheats.Kazumi.Core;
using Cheats.Kazumi.Handlers;
using Cheats.Kazumi.Objects;
using UnityEngine;

namespace Cheats.Kazumi.Commands {
    [CommandInfo(Cmd: "Enable", About:"Enabled a module")]
    public class EnableModule : Command{
        public override void OnRun(string[] args) {
            if (args.Length <= 1) {
                Console.Send(LogType.Error, "Required argument [<color=cyan>Command</color>] not found\n" +
                                            "Usage example: Enable <color=cyan> "+ModuleHandler.Modules.Keys.First() +"</color>");
                
            }
        }
    }
}